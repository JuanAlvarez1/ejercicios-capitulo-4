'''EJERCICIOS CAPITULO 4'''
#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"
'''Ejercicio 7: Reescribe el programa de calificaciones del capítulo anterior usando una función llamada
calcula_calificacion, que reciba una puntuación como parámetro y devuelva una calificación como cadena'''
'''Ubicamos las funciones antes que el codigo principal'''

def calcula_calificacion(ca):
    if ca >= 0  and ca  <=  1.0:
        if ca >=  0.9:
            print("Sobresaliente")
        elif ca >=  0.8:
            print("Notable")
        elif ca >=  0.7:
            print("Bien")
        elif ca <=  0.6:
            print("Insuficiente")
    else:
        print("Califiación no valida")

'''Codigo principal'''

try:
    puntuacion  =   float(input("Introduzca puntuacion ⬇⬇⬇ :\n"))
    calcula_calificacion(puntuacion)
except:
    print("Calificacion ingresada incorrecta")
